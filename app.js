let clickMenu = document.querySelector('.menu i');

clickMenu.addEventListener('click', openMenu);

function openMenu() {
    let menu = document.querySelector('.menu-open');
    menu.style.marginRight = menu.style.marginRight === '0px' ? '-250px' : '0px';
}